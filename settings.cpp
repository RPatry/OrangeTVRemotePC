#include "settings.hpp"
#include <QCoreApplication>
#include <QFile>
#include <QJsonDocument>

namespace {
char const* const settings_file_path { "settings.json" };
char const* const ip_address_key { "ip_address" };
char const* const port_key { "port" };

QString Get_Settings_File_Path() {
    QString format_string { "%1/%2" };
    return format_string.arg(QCoreApplication::applicationDirPath()).arg(settings_file_path);
}
}

Settings::Settings()
{
    this->Load_Settings_File();
}

QString Settings::Get_IP_Address() const {
    return this->ip_address;
}

void Settings::Set_IP_Address(QString value) {
    this->ip_address = value;
}

std::optional<QString> Settings::Get_Invalid_IP_Address_Error_Message() const {
    if (not this->ip_address.startsWith("192.168.1.")) {
        return "L'adresse IP n'est pas une adresse IP locale (doit commencer par \"192.168.1.\".";
    }
    const QStringList ip_bytes { this->ip_address.split('.', Qt::KeepEmptyParts) };
    if (ip_bytes.size() != 4) {
        return "L'adresse IP ne doit contenir que quatre valeurs.";
    }
    bool byte_conversion_okay { false };
    const int maybe_byte { ip_bytes.last().toInt(&byte_conversion_okay, 10) };
    if (not byte_conversion_okay) {
        return "La dernière valeur de l'adresse IP n'est pas un nombre entier";
    }
    if ((maybe_byte < 2) or (maybe_byte > 254)) {
        return "La dernière valeur de l'adresse IP doit être supérieure à 1 et ≤ 254.";
    }
    return std::nullopt;
}

int Settings::Get_Port() const {
    return this->port.toInt(nullptr, 10);
}

void Settings::Set_Port(QString value) {
    this->port = value;
}

std::optional<QString> Settings::Get_Invalid_Port_Error_Message() const {
    bool port_has_int { false };
    const int port { this->port.toInt(&port_has_int, 10) };
    if (not port_has_int) {
        return "Le numéro de port doit être un nombre entier (la valeur la plus probable est 8080).";
    }
    if ((port < 0) or (port > 65535)) {
        return "Le numéro de port est invalide, on doit avoir 0 ≤ port ≤ 65535 (la valeur la plus probable est 8080).";
    }
    return std::nullopt;
}

void Settings::Write_Settings_File() {
    QJsonObject settings_json_object { {ip_address_key, this->ip_address}, {port_key, this->port } };
    QJsonDocument settings_json_document (settings_json_object);
    const QByteArray settings_json_raw_contents { settings_json_document.toJson() };
    QFile output_settings_file { Get_Settings_File_Path() };
    if (output_settings_file.open(QIODevice::OpenModeFlag::WriteOnly)) {
        output_settings_file.write(settings_json_raw_contents);
    }
}

void Settings::Load_Settings_File() {
    QFile settings_file { Get_Settings_File_Path() };
    if (settings_file.open(QIODevice::OpenModeFlag::ReadOnly)) {
        const QByteArray settings_file_contents { settings_file.readAll() };
        QJsonDocument settings_file_json_document (QJsonDocument::fromJson(settings_file_contents, nullptr));
        if (settings_file_json_document.isObject()) {
            QJsonObject root_dictionary (settings_file_json_document.object());
            this->ip_address = root_dictionary.value(ip_address_key).toString();
            this->port = root_dictionary.value(port_key).toString();
        }
    }
}
