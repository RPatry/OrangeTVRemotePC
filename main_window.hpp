#ifndef ORANGE_TV_REMOTE_PC_MAINWINDOW_HPP
#define ORANGE_TV_REMOTE_PC_MAINWINDOW_HPP

#include <QMainWindow>
#include "settings.hpp"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow: public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void N0_Pushed();
    void N1_Pushed();
    void N2_Pushed();
    void N3_Pushed();
    void N4_Pushed();
    void N5_Pushed();
    void N6_Pushed();
    void N7_Pushed();
    void N8_Pushed();
    void N9_Pushed();
    void VolumePlus_Pushed();
    void VolumeMinus_Pushed();
    void Mute_Pushed();
    void ChannelPlus_Pushed();
    void ChannelMinus_Pushed();
    void OK_Pushed();
    void Menu_Pushed();
    void Back_Pushed();
    void Power_Pushed();
    void Backward_Pushed();
    void Pause_Play_Pushed();
    void Forwards_Pushed();
    void Up_Arrow_Pushed();
    void Down_Arrow_Pushed();
    void Left_Arrow_Pushed();
    void Right_Arrow_Pushed();

    void IP_Address_Changed(QString const& new_ip_address);
    void Port_Changed(QString const& new_port);
    void Detect_Decoder();

private:
    Ui::MainWindow *ui;

    Settings settings {};

    void Send_Command_To_Decoder(QString raw_command);
    QString Make_Request_URL(QString raw_command) const;
    QString Make_Decoder_Check_State_URL(int _192_168_1_x) const;
};
#endif // ORANGE_TV_REMOTE_PC_MAINWINDOW_HPP
