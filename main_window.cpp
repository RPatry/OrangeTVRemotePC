#include "main_window.hpp"
#include "./ui_main_window.h"
#include <QErrorMessage>
#include <QEventLoop>
#include <QJsonDocument>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow { parent }, ui { new Ui::MainWindow }
{
    ui->setupUi(this);

    ui->pushButton_Up->setFocus();
    ui->lineEdit_IP_Address_Decoder->setText(this->settings.Get_IP_Address());
    ui->lineEdit_Decoder_Port->setText(QString { "%1" }.arg(this->settings.Get_Port()));

    ui->label_Qt_Version_And_License->setText(
        QString { "Ce logiciel utilise Qt %1.%2.%3, et utilise la GPLv3 (code source: <a href=\"https://gitlab.com/RPatry/OrangeTVRemotePC\">https://gitlab.com/RPatry/OrangeTVRemotePC</a>)" }
        .arg(QT_VERSION_MAJOR).arg(QT_VERSION_MINOR).arg(QT_VERSION_PATCH));
    ui->label_Qt_Version_And_License->setTextFormat(Qt::TextFormat::RichText);
    ui->label_Qt_Version_And_License->setTextInteractionFlags(Qt::TextInteractionFlag::TextBrowserInteraction);

    ui->pushButton_N0->setToolTip("Touche 0 (raccourci clavier: " + ui->pushButton_N0->shortcut().toString() + ')');
    ui->pushButton_N1->setToolTip("Touche 1 (raccourci clavier: " + ui->pushButton_N1->shortcut().toString() + ')');
    ui->pushButton_N2->setToolTip("Touche 2 (raccourci clavier: " + ui->pushButton_N2->shortcut().toString() + ')');
    ui->pushButton_N3->setToolTip("Touche 3 (raccourci clavier: " + ui->pushButton_N3->shortcut().toString() + ')');
    ui->pushButton_N4->setToolTip("Touche 4 (raccourci clavier: " + ui->pushButton_N4->shortcut().toString() + ')');
    ui->pushButton_N5->setToolTip("Touche 5 (raccourci clavier: " + ui->pushButton_N5->shortcut().toString() + ')');
    ui->pushButton_N6->setToolTip("Touche 6 (raccourci clavier: " + ui->pushButton_N6->shortcut().toString() + ')');
    ui->pushButton_N7->setToolTip("Touche 7 (raccourci clavier: " + ui->pushButton_N7->shortcut().toString() + ')');
    ui->pushButton_N8->setToolTip("Touche 8 (raccourci clavier: " + ui->pushButton_N8->shortcut().toString() + ')');
    ui->pushButton_N9->setToolTip("Touche 9 (raccourci clavier: " + ui->pushButton_N9->shortcut().toString() + ')');
    ui->pushButton_Volume_Increase->setToolTip("Augmenter le volume (raccourci clavier: " + ui->pushButton_Volume_Increase->shortcut().toString() + ')');
    ui->pushButton_Decrease_Volume->setToolTip("Baisser le volume (raccourci clavier: " + ui->pushButton_Decrease_Volume->shortcut().toString() + ')');
    ui->pushButton_Mute->setToolTip("Couper/rétablir le son (raccourci clavier: " + ui->pushButton_Mute->shortcut().toString() + ')');
    ui->pushButton_Increase_Channel->setToolTip("Aller à la chaîne suivante (raccourci clavier: " + ui->pushButton_Increase_Channel->shortcut().toString() + ')');
    ui->pushButton_Decrease_Channel->setToolTip("Aller à la chaîne précédente (raccourci clavier: " + ui->pushButton_Decrease_Channel->shortcut().toString() + ')');
    ui->pushButton_OK->setToolTip("Touche OK (raccourci clavier: " + ui->pushButton_OK->shortcut().toString() + ')');
    ui->pushButton_Menu->setToolTip("Faire apparaître le menu Orange (raccourci clavier: " + ui->pushButton_Menu->shortcut().toString() + ')');
    ui->pushButton_Back->setToolTip("Touche retour, pour sortir d'un menu, revenir à la chaîne visitée précédemment, sortir du replay, etc. (raccourci clavier: " + ui->pushButton_Back->shortcut().toString() + ')');
    ui->pushButton_Power_On_Off->setToolTip("Allumer/éteindre la télé (raccourci clavier: " + ui->pushButton_Power_On_Off->shortcut().toString() + ')');
    ui->pushButton_Backwards->setToolTip("Retourner en arrière dans la vidéo, appuyer plusieurs fois pour accélérer le retour (raccourci clavier: " + ui->pushButton_Backwards->shortcut().toString() + ')');
    ui->pushButton_Play_Pause->setToolTip("Mettre la vidéo en pause/sortir de la pause (raccourci clavier: " + ui->pushButton_Play_Pause->shortcut().toString() + ')');
    ui->pushButton_Forwards->setToolTip("Avancer en avant dans la vidéo, appuyer plusieurs fois pour accélérer l'avancement (raccourci clavier: " + ui->pushButton_Forwards->shortcut().toString() + ')');
    ui->pushButton_Up->setToolTip("Touche haut (raccourci clavier: " + ui->pushButton_Up->shortcut().toString() + ')');
    ui->pushButton_Down->setToolTip("Touche haut (raccourci clavier: " + ui->pushButton_Down->shortcut().toString() + ')');
    ui->pushButton_Left->setToolTip("Touche haut (raccourci clavier: " + ui->pushButton_Left->shortcut().toString() + ')');
    ui->pushButton_Right->setToolTip("Touche haut (raccourci clavier: " + ui->pushButton_Right->shortcut().toString() + ')');

    ui->pushButton_Detect_Decoder->setToolTip("Tenter de détecter automatiquement l'emplacement réseau du décodeur (dans le cas habituel, c'est seulement utile lors de la toute première utilisation).");

    connect(ui->pushButton_N0, &QPushButton::clicked, this, &MainWindow::N0_Pushed);
    connect(ui->pushButton_N1, &QPushButton::clicked, this, &MainWindow::N1_Pushed);
    connect(ui->pushButton_N2, &QPushButton::clicked, this, &MainWindow::N2_Pushed);
    connect(ui->pushButton_N3, &QPushButton::clicked, this, &MainWindow::N3_Pushed);
    connect(ui->pushButton_N4, &QPushButton::clicked, this, &MainWindow::N4_Pushed);
    connect(ui->pushButton_N5, &QPushButton::clicked, this, &MainWindow::N5_Pushed);
    connect(ui->pushButton_N6, &QPushButton::clicked, this, &MainWindow::N6_Pushed);
    connect(ui->pushButton_N7, &QPushButton::clicked, this, &MainWindow::N7_Pushed);
    connect(ui->pushButton_N8, &QPushButton::clicked, this, &MainWindow::N8_Pushed);
    connect(ui->pushButton_N9, &QPushButton::clicked, this, &MainWindow::N9_Pushed);
    connect(ui->pushButton_Volume_Increase, &QPushButton::clicked, this, &MainWindow::VolumePlus_Pushed);
    connect(ui->pushButton_Decrease_Volume, &QPushButton::clicked, this, &MainWindow::VolumeMinus_Pushed);
    connect(ui->pushButton_Mute, &QPushButton::clicked, this, &MainWindow::Mute_Pushed);
    connect(ui->pushButton_Increase_Channel, &QPushButton::clicked, this, &MainWindow::ChannelPlus_Pushed);
    connect(ui->pushButton_Decrease_Channel, &QPushButton::clicked, this, &MainWindow::ChannelMinus_Pushed);
    connect(ui->pushButton_OK, &QPushButton::clicked, this, &MainWindow::OK_Pushed);
    connect(ui->pushButton_Menu, &QPushButton::clicked, this, &MainWindow::Menu_Pushed);
    connect(ui->pushButton_Back, &QPushButton::clicked, this, &MainWindow::Back_Pushed);
    connect(ui->pushButton_Power_On_Off, &QPushButton::clicked, this, &MainWindow::Power_Pushed);
    connect(ui->pushButton_Backwards, &QPushButton::clicked, this, &MainWindow::Backward_Pushed);
    connect(ui->pushButton_Play_Pause, &QPushButton::clicked, this, &MainWindow::Pause_Play_Pushed);
    connect(ui->pushButton_Forwards, &QPushButton::clicked, this, &MainWindow::Forwards_Pushed);
    connect(ui->pushButton_Up, &QPushButton::clicked, this, &MainWindow::Up_Arrow_Pushed);
    connect(ui->pushButton_Down, &QPushButton::clicked, this, &MainWindow::Down_Arrow_Pushed);
    connect(ui->pushButton_Left, &QPushButton::clicked, this, &MainWindow::Left_Arrow_Pushed);
    connect(ui->pushButton_Right, &QPushButton::clicked, this, &MainWindow::Right_Arrow_Pushed);

    connect(ui->lineEdit_IP_Address_Decoder, &QLineEdit::textChanged, this, &MainWindow::IP_Address_Changed);
    connect(ui->lineEdit_Decoder_Port, &QLineEdit::textChanged, this, &MainWindow::Port_Changed);
    connect(ui->pushButton_Detect_Decoder, &QPushButton::clicked, this, &MainWindow::Detect_Decoder);

    this->adjustSize();
}

MainWindow::~MainWindow() {
    delete ui;
}

// Decoder API documentation (for the command strings): https://gist.github.com/alexandrevilain/c74fd7dabe148c8a16092eba38267c63

void MainWindow::N0_Pushed() {
    this->Send_Command_To_Decoder("512");
}

void MainWindow::N1_Pushed() {
    this->Send_Command_To_Decoder("513");
}

void MainWindow::N2_Pushed() {
    this->Send_Command_To_Decoder("514");
}

void MainWindow::N3_Pushed() {
    this->Send_Command_To_Decoder("515");
}

void MainWindow::N4_Pushed() {
    this->Send_Command_To_Decoder("516");
}

void MainWindow::N5_Pushed() {
    this->Send_Command_To_Decoder("517");
}

void MainWindow::N6_Pushed() {
    this->Send_Command_To_Decoder("518");
}

void MainWindow::N7_Pushed() {
    this->Send_Command_To_Decoder("519");
}

void MainWindow::N8_Pushed() {
    this->Send_Command_To_Decoder("520");
}

void MainWindow::N9_Pushed() {
    this->Send_Command_To_Decoder("521");
}

void MainWindow::VolumePlus_Pushed() {
    this->Send_Command_To_Decoder("115");
}

void MainWindow::VolumeMinus_Pushed() {
    this->Send_Command_To_Decoder("114");
}

void MainWindow::Mute_Pushed() {
    this->Send_Command_To_Decoder("113");
}

void MainWindow::ChannelPlus_Pushed() {
    this->Send_Command_To_Decoder("402");
}

void MainWindow::ChannelMinus_Pushed() {
    this->Send_Command_To_Decoder("403");
}

void MainWindow::OK_Pushed() {
    this->Send_Command_To_Decoder("352");
}

void MainWindow::Menu_Pushed() {
    this->Send_Command_To_Decoder("139");
}

void MainWindow::Back_Pushed() {
    this->Send_Command_To_Decoder("158");
}

void MainWindow::Power_Pushed() {
    this->Send_Command_To_Decoder("116");
}

void MainWindow::Backward_Pushed() {
    this->Send_Command_To_Decoder("168");
}

void MainWindow::Pause_Play_Pushed() {
    this->Send_Command_To_Decoder("164");
}

void MainWindow::Forwards_Pushed() {
    this->Send_Command_To_Decoder("159");
}

void MainWindow::Up_Arrow_Pushed() {
    this->Send_Command_To_Decoder("103");
}

void MainWindow::Down_Arrow_Pushed() {
    this->Send_Command_To_Decoder("108");
}

void MainWindow::Left_Arrow_Pushed() {
    this->Send_Command_To_Decoder("105");
}

void MainWindow::Right_Arrow_Pushed() {
    this->Send_Command_To_Decoder("106");
}

void MainWindow::IP_Address_Changed(QString const& new_ip_address) {
    this->settings.Set_IP_Address(new_ip_address);
    QPalette palette { ui->lineEdit_IP_Address_Decoder->palette() };
    if (const std::optional error_message { this->settings.Get_Invalid_IP_Address_Error_Message() };
        error_message.has_value()) {
        ui->lineEdit_IP_Address_Decoder->setToolTip(*error_message);
        palette.setColor(QPalette::Base, Qt::red);
    }
    else {
        ui->lineEdit_IP_Address_Decoder->setToolTip("");
        palette.setColor(QPalette::Base, Qt::white);
    }
    ui->lineEdit_IP_Address_Decoder->setPalette(palette);
    this->settings.Write_Settings_File();
}

void MainWindow::Port_Changed(QString const& new_port) {
    this->settings.Set_Port(new_port);
    QPalette palette { ui->lineEdit_Decoder_Port->palette() };
    if (const std::optional error_message { this->settings.Get_Invalid_Port_Error_Message() };
        error_message.has_value()) {
        ui->lineEdit_Decoder_Port->setToolTip(*error_message);
        palette.setColor(QPalette::Base, Qt::red);
    }
    else {
        ui->lineEdit_Decoder_Port->setToolTip("");
        palette.setColor(QPalette::Base, Qt::white);
    }
    ui->lineEdit_Decoder_Port->setPalette(palette);
    this->settings.Write_Settings_File();
}

namespace {
QNetworkReply* Make_HTTP_GET_Request(QNetworkAccessManager& network_access_manager, QString request_url, int timeout_milliseconds = 2000) {
    network_access_manager.setTransferTimeout(timeout_milliseconds);
    QNetworkReply* reply { network_access_manager.get(QNetworkRequest { request_url }) };
    QEventLoop loop {};
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    // Wait until the reply is fully received (or until we receive an error, because the IP address is not affected to a device,
    // or does not correspond to a functional TV decoder).
    loop.exec();
    return reply;

}

bool JSON_Value_For_Key_Is_String(QJsonObject const& object, QString key, QString expected_value) {
    if (not object.contains(key)) {
        return false;
    }
    if (not object.value(key).isString()) {
        return false;
    }
    return object.value(key).toString() == expected_value;
}

bool Check_JSON_Reply_Looks_To_Come_From_Decoder(QJsonDocument document) {
    if (not document.isObject()) {
        return false;
    }
    QJsonObject root_object { document.object() };
    if ((not root_object.contains("result")) or (not root_object.value("result").isObject())) {
        return false;
    }
    QJsonObject result_object { root_object.value("result").toObject() };
    if (not JSON_Value_For_Key_Is_String(result_object, "responseCode", "0")) {
        return false;
    }
    if (not JSON_Value_For_Key_Is_String(result_object, "message", "ok")) {
        return false;
    }
    if ((not result_object.contains("data")) or (not result_object.value("data").isObject())) {
        return false;
    }
    // Probably not worth it to try and check the contents of the data dictionary, so if we make it this far, we assume we have received
    // a reply from a decoder.
    return true;
}
}

void MainWindow::Detect_Decoder() {
    QNetworkAccessManager network_access_manager {};
    const int ipv4_local_address_last_unique_device_byte { 254 };
    for (int i { 1 }; i <= ipv4_local_address_last_unique_device_byte; i++) {
        ui->statusbar->showMessage(QString { "Veuillez attendre: vérification si l'appareil à 192.168.1.%1 contient un décodeur..." }.arg(i));
        ui->statusbar->repaint();

        QNetworkReply* const reply { Make_HTTP_GET_Request(network_access_manager, this->Make_Decoder_Check_State_URL(i)) };
        if (reply->error() != QNetworkReply::NoError) {
            // The HTTP request failed, so (unless the decoder is dysfunctional), the current IP address is not for the device we are looking for.
            continue;
        }

        const QByteArray reply_contents { reply->readAll() };
        QJsonDocument reply_json_document { QJsonDocument::fromJson(reply_contents) };
        if (Check_JSON_Reply_Looks_To_Come_From_Decoder(reply_json_document)) {
            ui->lineEdit_IP_Address_Decoder->setText(QString { "192.168.1.%1" }.arg(i));  // This will trigger saving the new IP address to the settings.
            ui->statusbar->showMessage(QString { "La vérification a réussi: votre décodeur se trouve à %1" }.arg(ui->lineEdit_IP_Address_Decoder->text()));
            return;
        }
    }
    QErrorMessage error_message { this };
    error_message.showMessage("Il a été impossible de détecter un décodeur Orange sur ce réseau.");
    error_message.exec();
    ui->statusbar->clearMessage();
}

namespace {
QString Get_Readable_Network_Error_Representation(QNetworkReply::NetworkError network_error) {
    switch (network_error) {
        case QNetworkReply::NetworkError::NoError:
            return "NoError : Aucune erreur.";
        case QNetworkReply::NetworkError::ConnectionRefusedError:
            return "ConnectionRefusedError : Le serveur distant a refusé la connexion (le serveur n'accepte pas les requêtes).";
        case QNetworkReply::NetworkError::RemoteHostClosedError:
            return "RemoteHostClosedError : Le serveur distant a fermé la connexion prématurément, avant que la réponse complète ne soit reçue et traitée.";
        case QNetworkReply::NetworkError::HostNotFoundError:
            return "HostNotFoundError : Le nom d'hôte distant n'a pas été trouvé (nom d'hôte invalide).";
        case QNetworkReply::NetworkError::TimeoutError:
            return "TimeoutError : La connexion au serveur distant a expiré.";
        case QNetworkReply::NetworkError::OperationCanceledError:
            return "OperationCanceledError : L'opération a été annulée via des appels à abort() ou close() avant d'être terminée.";
        case QNetworkReply::NetworkError::SslHandshakeFailedError:
            return "SslHandshakeFailedError : La négociation SSL/TLS a échoué et le canal chiffré n'a pas pu être établi. Le signal sslErrors() aurait dû être émis.";
        case QNetworkReply::NetworkError::TemporaryNetworkFailureError:
            return "TemporaryNetworkFailureError : La connexion a été interrompue en raison d'une déconnexion du réseau, cependant le système a initié l'itinérance vers un autre point d'accès. La requête devrait être soumise à nouveau et sera traitée dès que la connexion sera rétablie.";
        case QNetworkReply::NetworkError::NetworkSessionFailedError:
            return "NetworkSessionFailedError : La connexion a été interrompue en raison d'une déconnexion du réseau ou d'un échec du démarrage du réseau.";
        case QNetworkReply::NetworkError::BackgroundRequestNotAllowedError:
            return "BackgroundRequestNotAllowedError : La requête en arrière-plan n'est actuellement pas autorisée en raison de la politique de la plateforme.";
        case QNetworkReply::NetworkError::TooManyRedirectsError:
            return "TooManyRedirectsError : Lors du suivi des redirections, la limite maximale a été atteinte. La limite est par défaut fixée à 50 ou selon la valeur définie par QNetworkRequest::setMaxRedirectsAllowed().";
        case QNetworkReply::NetworkError::InsecureRedirectError:
            return "InsecureRedirectError : Lors du suivi des redirections, l'API d'accès réseau a détecté une redirection d'un protocole chiffré (https) vers un protocole non chiffré (http).";
        case QNetworkReply::NetworkError::ProxyConnectionRefusedError:
            return "ProxyConnectionRefusedError : La connexion au serveur proxy a été refusée (le serveur proxy n'accepte pas les requêtes).";
        case QNetworkReply::NetworkError::ProxyConnectionClosedError:
            return "ProxyConnectionClosedError : Le serveur proxy a fermé la connexion prématurément, avant que la réponse complète ne soit reçue et traitée.";
        case QNetworkReply::NetworkError::ProxyNotFoundError:
            return "ProxyNotFoundError : Le nom d'hôte du proxy n'a pas été trouvé (nom d'hôte de proxy invalide).";
        case QNetworkReply::NetworkError::ProxyTimeoutError:
            return "ProxyTimeoutError : La connexion au proxy a expiré ou le proxy n'a pas répondu à temps à la requête envoyée.";
        case QNetworkReply::NetworkError::ProxyAuthenticationRequiredError:
            return "ProxyAuthenticationRequiredError : Le proxy nécessite une authentification pour honorer la requête mais n'a accepté aucune des informations d'identification proposées.";
        case QNetworkReply::NetworkError::ContentAccessDenied:
            return "ContentAccessDenied : L'accès au contenu distant a été refusé (similaire à l'erreur HTTP 403).";
        case QNetworkReply::NetworkError::ContentOperationNotPermittedError:
            return "ContentOperationNotPermittedError : L'opération demandée sur le contenu distant n'est pas autorisée.";
        case QNetworkReply::NetworkError::ContentNotFoundError:
            return "ContentNotFoundError : Le contenu distant n'a pas été trouvé sur le serveur (similaire à l'erreur HTTP 404).";
        case QNetworkReply::NetworkError::AuthenticationRequiredError:
            return "AuthenticationRequiredError : Le serveur distant nécessite une authentification pour servir le contenu mais les informations d'identification fournies n'ont pas été acceptées.";
        case QNetworkReply::NetworkError::ContentReSendError:
            return "ContentReSendError : La requête devait être envoyée à nouveau, mais cela a échoué, par exemple parce que les données à télécharger n'ont pas pu être lues une seconde fois.";
        case QNetworkReply::NetworkError::ContentConflictError:
            return "ContentConflictError : La requête n'a pas pu être complétée en raison d'un conflit avec l'état actuel de la ressource.";
        case QNetworkReply::NetworkError::ContentGoneError:
            return "ContentGoneError : La ressource demandée n'est plus disponible sur le serveur.";
        case QNetworkReply::NetworkError::InternalServerError:
            return "InternalServerError : Le serveur a rencontré une condition inattendue qui l'a empêché de satisfaire la requête.";
        case QNetworkReply::NetworkError::OperationNotImplementedError:
            return "OperationNotImplementedError : Le serveur ne prend pas en charge la fonctionnalité requise pour satisfaire la requête.";
        case QNetworkReply::NetworkError::ServiceUnavailableError:
            return "ServiceUnavailableError : Le serveur est actuellement incapable de traiter la requête.";
        case QNetworkReply::NetworkError::ProtocolUnknownError:
            return "ProtocolUnknownError : L'API d'accès réseau ne peut pas honorer la requête car le protocole est inconnu.";
        case QNetworkReply::NetworkError::ProtocolInvalidOperationError:
            return "ProtocolInvalidOperationError : L'opération demandée est invalide pour ce protocole.";
        case QNetworkReply::NetworkError::UnknownNetworkError:
            return "UnknownNetworkError : Une erreur réseau inconnue a été détectée.";
        case QNetworkReply::NetworkError::UnknownProxyError:
            return "UnknownProxyError : Une erreur inconnue liée au proxy a été détectée.";
        case QNetworkReply::NetworkError::UnknownContentError:
            return "UnknownContentError : Une erreur inconnue liée au contenu distant a été détectée.";
        case QNetworkReply::NetworkError::ProtocolFailure:
            return "ProtocolFailure : Une défaillance du protocole a été détectée (erreur d'analyse, réponses invalides ou inattendues, etc.).";
        case QNetworkReply::NetworkError::UnknownServerError:
            return "UnknownServerError : Une erreur inconnue liée à la réponse du serveur a été détectée.";
        default:
            return "Erreur non répertoriée.";
    }
}
}

void MainWindow::Send_Command_To_Decoder(QString raw_command) {
    QNetworkAccessManager network_access_manager {};
    QNetworkReply* reply { Make_HTTP_GET_Request(network_access_manager, this->Make_Request_URL(raw_command)) };
    // Wait until the decoder has replied to the command, or until sending the request has been considered a failure
    // (because of the wrong IP being picked, or because the decoder is dysfunctional, etc.).
    if (reply->error() != QNetworkReply::NoError) {
        QErrorMessage error_message { this };
        error_message.showMessage(QString { "Erreur lors de la tentative de communication avec le décodeur: %1" }
                                   .arg(Get_Readable_Network_Error_Representation(reply->error())));
        error_message.exec();
    }

    reply->deleteLater();
}

QString MainWindow::Make_Request_URL(QString raw_command) const {
    const QString url_format { "http://%1:%2/remoteControl/cmd?operation=01&key=%3&mode=0" };
    return url_format.arg(this->settings.Get_IP_Address()).arg(this->settings.Get_Port()).arg(raw_command);
}

QString MainWindow::Make_Decoder_Check_State_URL(int _192_168_1_x) const {
    const QString url_format { "http://192.168.1.%1:%2/remoteControl/cmd?operation=10" };
    return url_format.arg(_192_168_1_x).arg(this->settings.Get_Port());
}
