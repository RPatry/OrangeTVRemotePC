#ifndef ORANGE_TV_REMOTE_PC_SETTINGS_HPP
#define ORANGE_TV_REMOTE_PC_SETTINGS_HPP

#include <optional>
#include <QJsonObject>

struct Invalid_Setting_Value_Error {
    QString error_message {};
};

class Settings {
public:
    Settings();

    QString Get_IP_Address() const;
    void Set_IP_Address(QString value);
    std::optional<QString> Get_Invalid_IP_Address_Error_Message() const;
    int Get_Port() const;
    void Set_Port(QString value);
    std::optional<QString> Get_Invalid_Port_Error_Message() const;

    void Write_Settings_File();

private:
    QString ip_address { "192.168.1.10" };
    QString port { "8080" };

    void Load_Settings_File();
};

#endif // ORANGE_TV_REMOTE_PC_SETTINGS_HPP
